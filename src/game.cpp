#include "game.hpp"

namespace temp {
    Game::Game(){
        shape = sf::CircleShape(100);
    }

    Game::~Game(){}

    void Game::update(){
        checkKeyboard();

        setColor();

        last = current;
    }

    void Game::render(sf::RenderWindow &window){
        window.draw(shape);
    }

    void Game::checkKeyboard(){
        //play around with this code try getting the circle to stay green after release
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::F)){
            current = KeyType::PRESSED;
            return;
        }

        current = KeyType::RELEASED;
    }

    void Game::setColor(){
        if(current == KeyType::PRESSED){
            shape.setFillColor(sf::Color::Yellow);
            return;
        }

        if(current == KeyType::RELEASED && last == KeyType::RELEASED){
            shape.setFillColor(sf::Color::Red);
            return;
        }

        if(current == KeyType::RELEASED){
            shape.setFillColor(sf::Color::Green);
        }
    }
}