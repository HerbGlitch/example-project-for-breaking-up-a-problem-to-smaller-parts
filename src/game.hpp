#include <SFML/Graphics.hpp>
#include "keytype.hpp"

namespace temp {
    class Game {
    public:
        Game();
        ~Game();

        void update();
        void render(sf::RenderWindow &window);

    private:
        void checkKeyboard();

        void setColor();

        sf::CircleShape shape;

        KeyType current;
        KeyType last;
    };
}