#include <SFML/Graphics.hpp>
#include "game.hpp"

int main(){
    sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");

    temp::Game game;

    while(window.isOpen()){
        sf::Event event;
        while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed){
                window.close();
			}
        }

        game.update();

        window.clear();
        game.render(window);
        window.display();
    }

    return 0;
}