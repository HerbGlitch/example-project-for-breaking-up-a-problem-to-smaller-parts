#pragma once

enum class KeyType {
    PRESSED,
    RELEASED,
    NONE
};